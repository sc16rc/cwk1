// Class for COMP2931 Coursework 1

package comp2931.cwk1;


import java.util.Calendar;

/**
 * Slightly more than simple representation of a date.
 */
public class Date {

  //Class constants
  private static final int MONTHS_PER_YEAR = 12;
  private static final int[] DAYS_PER_MONTH = {31,28,31,30,31,30,31,31,30,31,30,31};
  private static final int[] LEAP_YEAR_DAYS = {31,29,31,30,31,30,31,31,30,31,30,31};

  private int year;
  private int month;
  private int day;

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   *
   * @throws IllegalArgumentException if month or day is invalid
   */
  public Date(int y, int m, int d) {

    if (m<1 || m>MONTHS_PER_YEAR){
      throw new IllegalArgumentException("Invalid month");
    }
    else if (d<1 || (!checkLeapYear(y) && d>DAYS_PER_MONTH[m-1])
            || (checkLeapYear(y) && d>LEAP_YEAR_DAYS[m-1])){
      throw new IllegalArgumentException("Invalid day");
    }
    else{
      year = y;
      month = m;
      day = d;
    }
  }

  /**
   * Creates a date using the current date
   */
  public Date(){
    Calendar calendar = Calendar.getInstance();

    year = calendar.get(Calendar.YEAR);
    month = calendar.get(Calendar.MONTH)+1;
    day = calendar.get(Calendar.DAY_OF_MONTH);

  }

  /**
   * Checks if a given year is a leap year
   *
   * <p>Leap years are divisible by 4 <em>except</em> if also divisible
   * by 100 and not divisble by 400 </p>
   *
   * @param year Year to be checked
   *
   * @return true if leap year, false if not
   */
  public static boolean checkLeapYear(int year){
    //Leap years are all divisible by 4
    if (year%4!=0){
      return false;
    }
    //1700, 1800, 1900 are leap years, 2000 is not
    else if (year%100==0 && year%400!=0){
      return false;
    }
    return true;
  }


  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides the number of days that a date is into the year.
   *
   * @return How many days into the year as  an integer
   */
  public int getDayOfYear(){
    int total=0;

    if (checkLeapYear(this.getYear())){
      for (int i=0; i<this.getMonth()-1;i++){
        total+=LEAP_YEAR_DAYS[i];
      }
    }
    else{
      for (int i=0; i<this.getMonth()-1;i++){
        total+=DAYS_PER_MONTH[i];
      }
    }


    total+=this.getDay();

    return total;
  }



  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  /**
   * Checks if this date is equal to another date.
   *
   * <p>The two objects are considered equal if both are instances of
   * the Date class <em>and</em> both represent exactly the same
   * date.</p>
   *
   * @return true if this Time object is equal to the other, false otherwise
   */
  @Override
  public boolean equals(Object other) {
    if (other == this) {
      // if comparing a Date object to itself (default equals method)
      return true;
    }
    else if (! (other instanceof Date)) {
      // if not comparing two Date object
      return false;
    }
    else {
      // Compare fields
      Date otherDate = (Date) other;
      return getYear() == otherDate.getYear()
              && getMonth() == otherDate.getMonth()
              && getDay() == otherDate.getDay();
    }
  }

}
