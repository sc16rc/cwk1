package comp2931.cwk1;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

import java.util.Calendar;


/**
 * Unit Tests for Date class
 */
public class DateTest {

    private Date fifthOct;


    @Before
    public void setUp() {
        fifthOct = new Date(2017,10,5);
    }

    @Test
    public void toStringTest() {
        assertThat(fifthOct.toString(), is("2017-10- 5"));
        assertThat(fifthOct.toString(), is(not("2017-10-05")));
    }

    @Test (expected = IllegalArgumentException.class)
    public void dayTooLow() {
        new Date(2017,10,0);
    }

    @Test (expected = IllegalArgumentException.class)
    public void dayTooHigh() {
        new Date(2017,10,32);
    }

    @Test (expected = IllegalArgumentException.class)
    public void monthTooLow() {
        new Date(2017,0,5);
    }

    @Test (expected = IllegalArgumentException.class)
    public void monthTooHigh() {
        new Date(2017,13,5);
    }

    @Test (expected = IllegalArgumentException.class)
    public void notALeapYear() {
        new Date(2017,2,29);
    }

    @Test
    public void isALeapYear() {
        Date leapYear = new Date(2016,2,29);
        assertThat(leapYear.getDay(), is(29));
    }


    @Test
    public void equality() {
        assertTrue(fifthOct.equals(fifthOct));
        assertTrue(fifthOct.equals(new Date(2017, 10, 5)));
        assertFalse(fifthOct.equals(new Date(1993, 10, 5)));
        assertFalse(fifthOct.equals(new Date(2017, 11, 5)));
        assertFalse(fifthOct.equals(new Date(2017, 10, 31)));

    }

    @Test
    public void dayOfTheYear(){
        assertThat(fifthOct.getDayOfYear(), is(278));
        assertThat(new Date(2017,1,1).getDayOfYear(), is(1));
        assertThat(new Date(2017,12,31).getDayOfYear(), is(365));
        assertThat(new Date(2016,12,31).getDayOfYear(), is(366));
    }

    @Test
    public void leapYear(){
        assertTrue(Date.checkLeapYear(2000));
        assertTrue(Date.checkLeapYear(2016));
        assertFalse(Date.checkLeapYear(2017));
        assertFalse(Date.checkLeapYear(1900));
    }

    @Test
    public void currentMonth(){
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();

        assertThat(today.getYear(), is(calendar.get(Calendar.YEAR)));
        assertThat(today.getMonth(), is(calendar.get(Calendar.MONTH)+1));
        assertThat(today.getDay(), is(calendar.get(Calendar.DAY_OF_MONTH)));

    }

}
